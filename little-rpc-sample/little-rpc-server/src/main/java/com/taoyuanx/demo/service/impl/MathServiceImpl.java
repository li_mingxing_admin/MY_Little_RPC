package com.taoyuanx.demo.service.impl;

import org.springframework.stereotype.Component;

import com.taoyuanx.demo.service.MathService;
import com.taoyuanx.littlerpc.anno.RpcService;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;

@RpcService(version="1",weight=100,token="123456")
@Component
public class MathServiceImpl implements MathService {

	@Override
	public Integer add(Integer a, Integer b) {
		return a+b;
	}

	@Override
	public FileInputStream fileDownload() {
		try {
			return new FileInputStream("D:\\gitSource\\TTT.txt");
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		System.out.println("没有找到这个文件。");
		return null;
	}

	@Override
	public byte[] fileDownloadByByte() {
		try {
			FileInputStream ins = new FileInputStream("D:\\gitSource\\ELK\\kibana-7.8.0-darwin-x86_64.tar.gz");
			byte[] byt = new byte[ins.available()];
			ins.read(byt);
			return byt;
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		System.out.println("没有找到这个文件。");
		return null;
	}

	@Override
	public Integer sub(Integer a, Integer b) {
		return a-b;
	}

	@Override
	public Integer mul(Integer a, Integer b) {
		return a*b;
	}

	@Override
	public Integer divide(Integer a, Integer b) {
		return a/b;
	}

}
