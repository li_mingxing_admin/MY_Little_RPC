package com.taoyuanx.demo.service;

import java.io.FileInputStream;

public interface MathService {
	Integer add(Integer a,Integer b);

	/**
	 * 新增加接口返回File对象。
	 * @return 文件对象
	 */
	FileInputStream fileDownload();

	byte[] fileDownloadByByte();

	Integer sub(Integer a,Integer b);
	Integer mul(Integer a,Integer b);
	Integer divide(Integer a,Integer b);
}
