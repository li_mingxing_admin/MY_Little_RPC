package com.taoyuanx.demo.client.main;

import org.apache.commons.io.IOUtils;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import com.taoyuanx.demo.client.controller.DemoController2;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;

public class MainClient {
    public static void main(String[] args) throws Exception {
        ClassPathXmlApplicationContext app = new ClassPathXmlApplicationContext("applicationContext.xml");
        DemoController2 demoController2 = app.getBean(DemoController2.class);
//		for(int i=0;i<100;i++) {
//			try {
//				System.out.println("add 结果:"+demoController2.add(0, i));
//				Thread.sleep(1000L);
//			} catch (Exception e) {
//				System.out.println("错误:");
//				e.printStackTrace();
//			}
//		}
//		FileInputStream fileIs = demoController2.fileDownload();
//		IOUtils.copy(fileIs, new FileOutputStream("D:\\gitSource\\little-rpc\\AAA.zip"));

        FileOutputStream fos = new FileOutputStream("D:\\gitSource\\little-rpc\\kibana-7.8.0-darwin-x86_64.tar.gz");
        byte[] bytes = demoController2.fileDownloadByByte();

        fos.write(bytes, 0, bytes.length);

        app.destroy();

    }
}
