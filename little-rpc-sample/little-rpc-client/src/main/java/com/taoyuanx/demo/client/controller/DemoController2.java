package com.taoyuanx.demo.client.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.taoyuanx.demo.service.MathService;
import com.taoyuanx.littlerpc.anno.RpcReference;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileWriter;

@Component
public class DemoController2 {
	@RpcReference(version="1", route="RoundRoute",timeout=1000)
	MathService mathService;
	public Integer add(Integer a, Integer b) {
		return mathService.add(a, b);
	}

	/**
	 * 不能完成，因为FileInputStream未执行序列化。
	 * @return
	 */
	public FileInputStream fileDownload() {
		return mathService.fileDownload();
	}
	public byte[] fileDownloadByByte() {
		return mathService.fileDownloadByByte();
	}

	public Integer sub(Integer a, Integer b) {
		return mathService.sub(a, b);
	}

	public Integer mul(Integer a, Integer b) {
		return mathService.sub(a, b);
	}

	public Integer divide(Integer a, Integer b) {
		return mathService.divide(a, b);
	}

}
